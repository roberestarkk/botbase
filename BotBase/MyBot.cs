﻿using Discord;
using Discord.Commands;

using System;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Web;
using System.Net;

namespace BotBase
{
    class MyBot
    {
        // Reference to the Discord client
        DiscordClient discord;
        CommandService commands;

        String botToken = "";

        String catAPI = "http://thecatapi.com/api/images/get?format=src";

        // C-Tor
        public MyBot()
        {
            // Create a new client instance with logging enabled
            discord = new DiscordClient(x =>
            {
                x.LogLevel = LogSeverity.Info;
                x.LogHandler = Log;
            });

            // Configure the bot to allow and respond to commands
            discord.UsingCommands(x =>
            {
                x.PrefixChar = ';';
                x.AllowMentionPrefix = true;
            });

            // 'Create' the commands to respond to
            createCommands();

            // Read the configuration file
            loadConfigs();
            

            // Connect to the server
            discord.ExecuteAndWait(async () =>
            {
                await discord.Connect(botToken, TokenType.Bot);
            });
        }

        public void loadConfigs()
        {
            // Store the current directory for re-setting later
            String dir = Directory.GetCurrentDirectory();
            
            // Get the actual base directory, which is the parent of the parent of the current
            DirectoryInfo newDir = Directory.GetParent(dir);
            newDir = newDir.Parent;

            // Set the current to the base
            Directory.SetCurrentDirectory(newDir.FullName);
            try
            {
                // Try to setup an xml reader on the application.cfg file
                XmlReader reader = XmlReader.Create("application.cfg");
                // Read through the whole file
                while (reader.Read())
                {
                    // If the current element is the 'token' element
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "token")
                    {
                        // Grab it's value
                        reader.Read();
                        // Set the value to the botToken variable
                        botToken = reader.Value;
                    }
                }
            }
            catch (FileNotFoundException)
            {
                // If you can't setup the reader with application.cfg because the FileNotFoundException
                // Make a copy of the configTemplate.cfg named application.cfg
                File.Copy("configTemplate.cfg", "application.cfg");
                // Warn the user to set up their application.cfg
                Console.WriteLine("Please configure the application.cfg file before re-running");
                // Terminate the program
                System.Environment.Exit(1);
            }
            // Reset the current directory to what it was before
            Directory.SetCurrentDirectory(dir);
        }

        // Takes a reference to the command service and adds commands to it
        public void createCommands()
        {
            // Reference to the command service
            commands = discord.GetService<CommandService>();

            // Create the Hello command
            commands.CreateCommand("ping")
                .Do(async (a) =>
                {
                    // eventually send a message to the channel responding to the command
                    await a.Channel.SendMessage("Pong");
                });

            // Create the cat command
            commands.CreateCommand("cat")
                .Alias(new String[] {"meow","kitty"})
                .Parameter("Type",ParameterType.Optional)
                .Parameter("Number",ParameterType.Optional)
                .Do(async (a) =>
                {
                    var type = "";
                    var num = 1;

                    // Default values for type and number argument index
                    int typeIndex = 0;
                    int numIndex = 1;

                    int output;
                    if (a.GetArg(0) != "")
                    {
                        if (int.TryParse(a.GetArg(0), out output))
                        {
                            numIndex = 0;
                        }
                    }

                    if (a.GetArg(1) != "")
                    {
                        if (!int.TryParse(a.GetArg(1), out output))
                        {
                            typeIndex = 1;
                        }
                    }
                    Console.WriteLine("Checking Type @ arg: " + typeIndex);
                    switch (a.GetArg(typeIndex))
                    {
                        case "gif":
                            type = "&type=gif";
                            break;
                        case "pic":
                            type = "&type=jpg,png";
                            break;
                        default:
                            break;
                    }
                    Console.WriteLine("Checking Number @ arg: " + numIndex);
                    switch (a.GetArg(numIndex))
                    {
                        case "":
                            break;
                        default:
                            num = int.Parse(a.GetArg(numIndex));
                            break;
                    }
                    Console.WriteLine("Getting cats " + num + " times with type: " + type);
                    for (int i = 0; i < num; i++)
                    {
                        WebRequest wrGET;
                        wrGET = WebRequest.Create(catAPI + type);

                        WebResponse response = wrGET.GetResponse();
                        if (response.ResponseUri.ToString() != null)
                        {
                            await a.Channel.SendMessage(response.ResponseUri.ToString());
                        }
                        else
                        {
                            await a.Channel.SendMessage("thecatapi.com failed to respond in time, please try again");
                        }
                        response.Close();
                    }
                });
        }

        // Used for logging to the command line
        private void Log(object sender, LogMessageEventArgs e)
        {
            Console.WriteLine(e.Message);
        }
    }
}
